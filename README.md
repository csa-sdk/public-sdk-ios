<div id="top"></div>

<!-- PROJECT LOGO -->
<div align="center">
  <a href="https://gitlab.com/csa-sdk/public-sdk-ios/-/raw/main/images/logo.png">
    <img src="images/logo.png" height="110" width="270">
  </a>
  <h2 align="center">Total Customer Engagement</h2>
  <p align="center"> https://www.coniq.com <br/>
  </p>
</div>

# Coniq - iOS SDK

### SDK Documentation
- https://ios-sdk.netlify.app/documentation/coniq

### APIs Documentation
- https://documenter.getpostman.com/view/4063684/RWaDWrYa

### SDK architecture

<div align="center">
  <a href="https://gitlab.com/csa-sdk/public-sdk-ios/-/raw/main/images/ConiqSDK.drawio.png">
    <img src="images/ConiqSDK.drawio.png">
  </a>    
  </p>
</div>

### Requirements

- iOS 15.0+
- Xcode 14+ 

### How to integrate the **Coniq.xcframework** into the iOS project:

Download the [Coniq.xcframework](https://gitlab.com/csa-sdk/public-sdk-ios/-/archive/main/public-sdk-ios-main.zip?path=Coniq.xcframework) from https://gitlab.com/csa-sdk/public-sdk-ios.

**On your Xcode project:**
1. Select your project in the Navigator Panel
2. Select the target for which you want to add the Coniq SDK
3. Select the *General* tab
4. Drag & drop the downloaded **Coniq.xcframework** to the *Frameworks, Libraries, and Embedded Content*
5. Now you can ***import Coniq*** into any file you want to use the SDK.

<div align="center">
  <a href="https://gitlab.com/csa-sdk/public-sdk-ios/-/raw/main/images/xcode_setup.png">
    <img src="images/xcode_setup.png">
  </a>    
  </p>
</div>


### Usage example

Open the **ConiqExample.xcodeproj** inside **EXAMPLE PROJECT** folder, especially check the the file <a href="https://gitlab.com/csa-sdk/public-sdk-ios/-/blob/main/EXAMPLE%20PROJECT/ConiqExample/ConiqSdkTest.swift">ConiqSdkTest.swift</a> 

<br />
<div align="center">
  <a href="https://gitlab.com/csa-sdk/public-sdk-ios/-/raw/main/images/ussage_example.pngpng">
    <img src="images/ussage_example.png">
  </a>    
  </p>
</div>

<p align="right">(<a href="#top">back to top</a>)</p>

#### Notice from the SDK developer:
- We haven't had a way to test the SDK for all possible cases and scenarios, so we assume the possibility that some things may not work as expected. If you discover any errors or omissions, please get in touch with the Coniq staff to contact us to fix the reported issue.
