//
//  ConiqExampleApp.swift
//  ConiqExample
//
//  Created by Senocico Stelian on 19.10.2022.
//

import SwiftUI

@main
struct ConiqExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

extension UIApplication {
    func dismissKeyboard() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
