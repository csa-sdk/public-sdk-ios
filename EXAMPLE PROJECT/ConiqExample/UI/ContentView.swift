//
//  ContentView.swift
//  ConiqExample
//
//  Created by Senocico Stelian on 19.10.2022.
//

import SwiftUI
import Combine
import Coniq

/**
 Demo implementation for testing the ConiqSDK framework
 
 IMPORTANT
 
 This implementation does not outline how to use the SDK as "this is how it should be done", but rather is just a demo implementation of how the SDK can be used.
 The SDK architecture is based on Combine, so you can use all the capabilities of this framework in any achitecture or custom implementation you may have.
 
 It performs very well with SwiftUI but can be used just as well on UIKit.
 */
struct ContentView: View {
    
    // SDK
    @ObservedObject private var coniqTest = ConiqSdkTest()
    private var cancellables: Set<AnyCancellable> = []
    @State private var showConsoleLogs = true
    
    // Alert state
    @State private var alertVisible = false
    
    // LOGIN
    @State private var inProgress: [String: Bool] = [:]
    @State private var email = ""
    @State private var password = ""
    @State private var newPassword = ""
    
    // REGISTER
    @State private var rFirstName = ""
    @State private var rEmail = ""
    @State private var rPassword = ""
    
    // CUSTOMER
    @State private var resetEmail = ""
    @State private var checkCustomerEmail = ""
    @State private var showReceiptSheet = false
    @State private var receiptCameraImage = UIImage()
    
    @State private var customerUpdateFirstName = ""
    @State private var customerUpdateLastName = ""
    @State private var customerUpdateAddress1 = ""
    @State private var saveToken = ""
    
    @State private var nPermission = false
    @State private var lPermission = false
    @State private var cPermission = false
    
    // PAYMENT
    @State private var paymentSuccessUrl = "https://www.something.com"
    @State private var paymentCancelUrl = "https://www.somethingElse.org"
    
    var body: some View {
        ScrollView {
            VStack {
                Group {
                    Image("logo")
                        .resizable()
                        .frame(width: 150, height: 60)
                        .padding()
                    
                    Toggle("Show Xcode Console API logs", isOn: $showConsoleLogs)
                        .onChange(of: showConsoleLogs, perform: { newValue in
                            coniqTest.printConsoleApiLogs(show: newValue)
                        })
                        .padding()
                }
                Group {
                    separator()
                    separator(title: "Retrieve SignUp Form & Register new customer")
                    button(title: "Retrieve SignUp Definition Form", action: {
                        coniqTest.testRetrieveSignUpDefinitionForm(apiHash: "0e5fe9s")
                    })
                    Group {
                        separator()
                        input(placeholder: "First Name", bindTo: $rFirstName)
                        input(placeholder: "Email", bindTo: $rEmail)
                        input(placeholder: "Password", bindTo: $rPassword)
                        button(title: "Register & Set password", alignment: .center, action: {
                            var newCustomer = NewCustomer(firstName: rFirstName, email: rEmail)
                            newCustomer.phone = Phone(countryCode: "40", number: "1234567890")
                            newCustomer.preferredLanguage = .EN
                            /// IMPORTANT: for the newCustomer object, you can set all the other properties you may need based on your apiHash
                            coniqTest.testRegisterAndSetCredentials(apiHash: "0e5fe9s", newCustomer: newCustomer, password: rPassword)
                        })
                    }
                }
                Group {
                    separator()
                    separator(title: "Check customer exists")
                    input(placeholder: "Enter customer email", bindTo: $checkCustomerEmail)
                    button(title: "Check email", alignment: .center, action: {
                        coniqTest.testCustomerExists(email: checkCustomerEmail)
                    })
                }
                Group {
                    separator()
                    separator(title: "Login customer & Update password")
                    input(placeholder: "Enter email", bindTo: $email)
                    input(placeholder: "Enter password", bindTo: $password)
                    input(placeholder: "New password", bindTo: $newPassword)
                    button(title: "Login & Update Password", alignment: .center, action: {
                        coniqTest.testLoginAndUpdatePassword(email: email, password: password, newPassword: newPassword)
                    })
                }
                Group {
                    separator()
                    separator(title: "Reset customer credentials")
                    input(placeholder: "Email address to send the password reset link", bindTo: $resetEmail)
                    button(title: "Send email", alignment: .center, action: {
                        coniqTest.testResetCustomerCredentials(email: resetEmail)
                    })
                }
                Group {
                    separator()
                    separator(title: "App - unauthenticated customer")
                    button(title: "Get app translations", action: {
                        coniqTest.testGetTranslations(forCustomer: false)
                    })
                    Group {
                        button(title: "Get schemes & Referral scheme", action: coniqTest.testGetSchemesAndRefferalScheme)
                        button(title: "Get subscription barcode", action: coniqTest.testCustomerGetSubscriptionBasic)
                        button(title: "Get loyalty program", action: coniqTest.testGetLoyaltyProgram)
                        button(title: "Get program offers", action: coniqTest.testGetProgramOffers)
                        button(title: "Get boosters", action: coniqTest.testGetBoosters)
                        button(title: "Get rewards", action: coniqTest.testGetProgramRewards)
                        button(title: "Get locations", action: coniqTest.testGetProgramLocations)
                        button(title: "Get location groups", action: coniqTest.testGetProgramLocationGroups)
                    }
                    Group {
                        separator()
                        separator(title: "Event - unauthenticated customer")
                        button(title: "Post external event", action: coniqTest.testPostExternalBarcodeEvent)
                    }
                }
                Group {
                    Group {
                        separator()
                        separator(title: "Authenticated customer - Update profile")
                        input(placeholder: "New first name", bindTo: $customerUpdateFirstName)
                        input(placeholder: "New last name", bindTo: $customerUpdateLastName)
                        input(placeholder: "New address", bindTo: $customerUpdateAddress1)
                        button(title: "Update profile", alignment: .center, action: {
                            coniqTest.testUpdateCustomerProfile(firstName: customerUpdateFirstName, lastName: customerUpdateLastName, address1: customerUpdateAddress1)
                        })
                    }
                    Group { /// a single group does not allow more than 10 subviews, so (just for testing) use Group in Group structure to get rid of the compile error
                        Group {
                            separator()
                            separator(title: "Authenticated customer")
                        }
                        button(title: "Get receipts", action: coniqTest.testGetCustomerReceipts)
                        button(title: "Post new receipt", openCameraPicker: true, action: { })
                        button(title: "Get subscription", action: coniqTest.testCustomerGetSubscription)
                        button(title: "Get redeemed offers", action: coniqTest.testGetRedeemedOffers)
                        button(title: "Get favorite offers", action: coniqTest.testGetCustomerFavoriteOffers)
                        button(title: "Redeem Spend Linked Offer", action: coniqTest.testGetLoyaltyProgramAndRedeemSpendLinkedOffer)
                        button(title: "Save favorite offer", action: coniqTest.testGetCustomerProgramOffersAndAddFavoriteOffer)
                    }
                    Group {
                        button(title: "Get orders", action: coniqTest.testGetCustomerOrders)
                        button(title: "Post visit", action: coniqTest.testCustomerPostVisit)
                    }
                    Group {
                        separator()
                        separator(title: "Authenticated customer - Program")
                        button(title: "Get program points events", action: coniqTest.testGetProgramPointsEvents)
                        button(title: "Get program available offers", action: coniqTest.testGetProgramAvailableOffers)
                    }
                    Group {
                        Group {
                            separator()
                            separator(title: "Authenticated customer - save token")
                            input(placeholder: "Token", bindTo: $saveToken)
                            button(title: "Save token", alignment: .center, action: {
                                coniqTest.testCustomerSaveToken(saveToken)
                            })
                        }
                        Group {
                            separator()
                            separator(title: "Authenticated customer - save device settings")
                            VStack {
                                Toggle("Notifications permissions", isOn: $nPermission).font(.system(size: 15)).foregroundColor(.teal)
                                Toggle("Location permissions", isOn: $lPermission).font(.system(size: 15)).foregroundColor(.teal)
                                Toggle("Camera permissions", isOn: $cPermission).font(.system(size: 15)).foregroundColor(.teal)
                            }
                            button(title: "Save settings", alignment: .center, action: {
                                coniqTest.testCustomerSaveDeviceSettings(nPermission: nPermission, lPermission: lPermission, cPermission: cPermission)
                            })
                        }
                    }
                    Group {
                        separator()
                        separator(title: "Authenticated customer - Payment")
                        input(placeholder: "Payment succes URL", bindTo: $paymentSuccessUrl)
                        input(placeholder: "Payment cancel URL", bindTo: $paymentCancelUrl)
                        button(title: "Initiate Payment", alignment: .center, action: {
                            coniqTest.testGetBoostersAndInitiatePayment(successUrl: paymentSuccessUrl, cancelUrl: paymentCancelUrl)
                        })
                        separator()
                        button(title: "Get customer payment redirect url", action: coniqTest.testGetCustomerPaymentRedirectUrl)
                        button(title: "Get customer linked payments", action: coniqTest.testGetCustomerLinkedPayments)
                    }
                    Group {
                        separator()
                        separator(title: "Authenticated customer - Delete account & Logout")
                        button(title: "Obfuscate", action: coniqTest.testCustomerObfuscate)
                        button(title: "Logout", action: coniqTest.testCustomerLogout)
                    }
                }
            }
            .padding()
        }
        
        // ALERT
        .alert(isPresented: $alertVisible) {
            Alert(
                title: Text(coniqTest.alertDetails.title),
                message: Text(coniqTest.alertDetails.description)
            )
        }
        
        // CAMERA SHEET
        .sheet(isPresented: $showReceiptSheet) {
            ImagePicker(sourceType: .camera, imageSelected: { image in
                coniqTest.testPostCustomer(receipt: image)
            })
        }
        
        // ON RECEIVE ALERT DATA
        .onReceive(coniqTest.$alertDetails) { alertDetails in
            alertVisible = !alertDetails.title.isEmpty || !alertDetails.description.isEmpty
            inProgress = [:]
        }
    }
}

private extension ContentView {
    
    func button(title: String,
                alignment: Alignment = .leading,
                openCameraPicker: Bool = false,
                action: @escaping (() -> Void)) -> some View {
        HStack {
            if inProgress[title] == true {
                ProgressView()
            }
            Button(action: {
                UIApplication.shared.dismissKeyboard()
                inProgress[title] = true
                if openCameraPicker {
                    showReceiptSheet = true
                } else {
                    action()
                }
            }) { Text(title) }
                .padding(12)
                .font(.system(size: 15))
                .disabled(inProgress[title] == true)
        }.frame(maxWidth: .infinity, alignment: alignment)
    }
    
    func input(placeholder: String, bindTo: Binding<String>) -> some View {
        TextField(placeholder, text: bindTo)
            .padding(.horizontal, 10)
            .font(.system(size: 15))
            .textFieldStyle(.roundedBorder)
    }
    
    func separator(title: String? = nil) -> some View {
        VStack {
            if let title = title {
                Text(title)
                    .font(.system(size: 13))
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            Divider()
                .frame(height: 0.5)
                .background(.white)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
