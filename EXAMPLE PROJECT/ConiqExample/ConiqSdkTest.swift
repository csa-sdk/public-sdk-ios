//
//  ConiqSdkTest.swift
//  ConiqExample
//
//  Created by Senocico Stelian on 19.10.2022.
//

import Coniq
import Combine
import UIKit

/**
 Demo implementation for testing the ConiqSDK framework
 
 IMPORTANT
 
 This implementation does not outline how to use the SDK as "this is how it should be done", but rather is just a demo implementation of how the SDK can be used.
 The SDK architecture is based on Combine, so you can use all the capabilities of this framework in any achitecture or custom implementation you may have.
 
 It performs very well with SwiftUI but can be used just as well on UIKit.
 */
final class ConiqSdkTest: ObservableObject {
    
    let coniq: Coniq
    
    private var cancellables: Set<AnyCancellable> = []
    
    let testProgramKey = "MeadowRewards"
    let testBarcode = "N7VNKQR7F6WDGHEY"
    
    @Published private(set) var alertDetails = (title: "", description: "")
    
    init() {
        let clientId = "107_1orcmg96ru4kg4csso080oocw8coks4k4g44ckcs4sk04wgg08"
        let clientSecret = "9895dz9nb7s4gwkgs4kgoggckwcc80oo8oogg8w0kwcsoosc4"
        
        let area: Environment.Area = .EU
        let env: Environment = .production(clientId: clientId, clientSecret: clientSecret, area: area)
        let configs = Coniq.Configurations(printConsoleApiLogs: true)
        coniq = Coniq(environment: env, configurations: configs)
    }
}

// MARK: - Public methods

extension ConiqSdkTest {
    
    /// Print or not API logs in Xcode Console
    func printConsoleApiLogs(show: Bool) {
        coniq.changeConfigurations(with: Coniq.Configurations(printConsoleApiLogs: show))
    }
    
    // MARK: // ** REGISTER ** //
    
    /// Test retrieve the sign up definition form
    func testRetrieveSignUpDefinitionForm(apiHash: String) {
        coniq.authentication.retrieveSignUpDefinitionForm(hash: apiHash).sink { retrieveSignUpFormState in
            self.setResponseAlert(requestState: retrieveSignUpFormState, description: "Retrieve Sign Up Definition Form")
        }.store(in: &cancellables)
    }
    
    /// Test register a new customer account and set credentials on success account creation
    func testRegisterAndSetCredentials(apiHash: String, newCustomer: NewCustomer, password: String) {
        coniq.authentication.register(apiHash: apiHash, newCustomer: newCustomer).sink { [unowned self] state in
            switch state {
            case .success(let registeredCustomer):
                coniq.customer.setPassword(for: registeredCustomer, password: password).sink { registerCustomerState in
                    self.setResponseAlert(requestState: registerCustomerState, description: "Register & Set Credentials")
                }.store(in: &cancellables)
                
            case .failure(let err):
                self.setErrorAlert(err)
                
            default:
                break
            }
        }.store(in: &cancellables)
    }
    
    // MARK: // ** LOGIN ** //
    
    /// Test social login
    func testSocialLogin(type: SocialType, email: String, password: String) {
        coniq.authentication.socialLogin(type: type, email: email, password: password).sink { socialLoginState in
            self.setResponseAlert(requestState: socialLoginState, description: "Social \(type.rawValue.uppercased()) login")
        }.store(in: &cancellables)
    }
    
    /// Test get a customer profile and update the customer password on success response
    func testLoginAndUpdatePassword(email: String, password: String, newPassword: String) {
        coniq.authentication.login(email: email, password: password).sink { [unowned self] state in
            switch state {
            case .success(let customer):
                coniq.customer.updatePassword(for: customer, currentPassword: password, newPassword: newPassword).sink { updatePasswordState in
                    self.setResponseAlert(requestState: updatePasswordState, description: "Login & Update Password")
                }.store(in: &cancellables)
            case .failure(let err):
                setErrorAlert(err)
                
            default: break
            }
        }.store(in: &cancellables)
    }
    
    // MARK: // ** CUSTOMER ** //
    
    /// Test customer exists
    func testCustomerExists(email: String) {
        coniq.customer.exists(email: email).sink { customerExistState in
            self.setResponseAlert(requestState: customerExistState, description: "Customer Exists")
        }.store(in: &cancellables)
    }
    
    /// Test update customer profile
    func testUpdateCustomerProfile(firstName: String, lastName: String, address1: String) {
        var updateCustomer = UpdateCustomer()
        updateCustomer.firstName = firstName
        updateCustomer.lastName = lastName
        updateCustomer.address1 = address1
        coniq.customer.updateProfile(newProfile: updateCustomer).sink { customerUpdateProfileState in
            self.setResponseAlert(requestState: customerUpdateProfileState, description: "Customer Update Profile")
        }.store(in: &cancellables)
    }
    
    /// Test customer reset credentials (forgot password)
    func testResetCustomerCredentials(email: String) {
        coniq.authentication.resetCredentials(email: email).sink { resetCredentialsStatus in
            self.setResponseAlert(requestState: resetCredentialsStatus, description: "Customer Reset Credentials")
        }.store(in: &cancellables)
    }
    
    /// Test get customer basic subscription information
    func testCustomerGetSubscriptionBasic() {
        coniq.customer.getBasicSubscription(programKey: testProgramKey, barcode: testBarcode).sink { subscriptionsState in
            self.setResponseAlert(requestState: subscriptionsState, description: "Get Customer Subscription Basic")
        }.store(in: &self.cancellables)
    }
    
    /// Test get customer subscription full details
    func testCustomerGetSubscription() {
        coniq.customer.getSubscription(programKey: testProgramKey).sink { subscriptionsState in
            self.setResponseAlert(requestState: subscriptionsState, description: "Get Customer Subscription")
        }.store(in: &self.cancellables)
    }
    
    /// Test get customer loyalty program
    func testGetLoyaltyProgram() {
        coniq.program.getLoyaltyProgram(programKey: testProgramKey).sink { loyaltyProgramState in
            self.setResponseAlert(requestState: loyaltyProgramState, description: "Get Loyalty Program")
        }.store(in: &self.cancellables)
    }
    
    /// Test get customer Loyalty Program and redeem spend linked offer
    func testGetLoyaltyProgramAndRedeemSpendLinkedOffer() {
        coniq.program.getLoyaltyProgram(programKey: testProgramKey).sink { loyaltyProgramState in
            switch loyaltyProgramState {
            case .success(let loyaltyProgram):
                if let firstRule = loyaltyProgram.pointsSpendLinkedOfferRules.first, let locId = firstRule.offer.locationIds.first {
                    self.coniq.program.spendRedeemLinkedOffer(programKey: self.testProgramKey, ruleId: firstRule.ruleId, amount: 1.0, locationId: locId).sink { redeemOfferState in
                        self.setResponseAlert(requestState: redeemOfferState, description: "Get Customer Loyalty Program & Redeem Offer")
                    }.store(in: &self.cancellables)
                } else {
                    self.setResponseAlert(requestState: loyaltyProgramState, description: "Get Customer Loyalty Program")
                }
                
            case .failure(let error):
                self.setErrorAlert(error)
                
            default:
                break
            }
            
            
            self.setResponseAlert(requestState: loyaltyProgramState, description: "Get Customer Loyalty Program")
        }.store(in: &self.cancellables)
    }
    
    /// Test get customer orders
    func testGetCustomerOrders() {
        coniq.customer.getOrders().sink { getOrdersState in
            self.setResponseAlert(requestState: getOrdersState, description: "Get Customer Orders")
        }.store(in: &cancellables)
    }
    
    /// Test get customer favorite offers
    func testGetCustomerFavoriteOffers() {
        coniq.customer.getFavoriteOffersIds(programKey: testProgramKey).sink { favOffersState in
            self.setResponseAlert(requestState: favOffersState, description: "Get Customer Favorite Offers")
        }.store(in: &cancellables)
    }
    
    /// Test get redeemed offers
    func testGetRedeemedOffers() {
        coniq.customer.getRedeemedOffers(programKey: testProgramKey).sink { getOffersState in
            self.setResponseAlert(requestState: getOffersState, description: "Get Customer Redeemed Offers")
        }.store(in: &cancellables)
    }
    
    /// Test get schemes and get refferal scheme
    func testGetSchemesAndRefferalScheme() {
        coniq.app.getSchemes().sink { getSchemesState in
            switch getSchemesState {
            case .success(let scheme):
                if let refPublicId = scheme.referralSchemes.first?.publicId {
                    self.coniq.app.getScheme(publicId: refPublicId).sink { getRefSchemeState in
                        self.setResponseAlert(requestState: getRefSchemeState, description: "Get app schemes & Referral scheme")
                    }.store(in: &self.cancellables)
                } else {
                    self.setResponseAlert(requestState: getSchemesState, description: "Get app schemes")
                }
                
            case .failure(let error):
                self.setErrorAlert(error)
                
            default:
                break
            }
            
        }.store(in: &cancellables)
    }
    
    /// Test get translations
    func testGetTranslations(forCustomer: Bool) {
        if forCustomer {
            coniq.customer.getTranslations().sink { getTranslationsState in
                self.setResponseAlert(requestState: getTranslationsState, description: "Get customer translations")
            }.store(in: &cancellables)
        } else {
            coniq.app.getTranslations().sink { getTranslationsState in
                self.setResponseAlert(requestState: getTranslationsState, description: "Get app translations")
            }.store(in: &cancellables)
        }
    }
    
    /// Test get program offers
    func testGetProgramOffers() {
        coniq.program.getProgramOffers(programKey: testProgramKey).sink { programOffersState in
            self.setResponseAlert(requestState: programOffersState, description: "Get program offers")
        }.store(in: &cancellables)
    }
    
    /// Test get customer program offers & Add the first offer to the favourites
    func testGetCustomerProgramOffersAndAddFavoriteOffer() {
        // then get the customer program offers for the first programKey
        coniq.program.getProgramOffers(programKey: testProgramKey).sink { offersRequestState in
            switch offersRequestState {
            case .success(let offers):
                // then add the first offer to the favorites (if exists)
                if let firstOffer = offers.first {
                    self.coniq.customer.saveFavoriteOffer(programKey: self.testProgramKey, offerId: firstOffer.id).sink { saveFavOfferRequestState in
                        self.setResponseAlert(requestState: saveFavOfferRequestState, description: "Get Customer Program Offers & Save Offer")
                    }.store(in: &self.cancellables)
                } else {
                    self.setResponseAlert(requestState: offersRequestState, description: "Get Customer Program Offers")
                }
                
            case .failure(let error):
                self.setErrorAlert(error)
                
            default:
                break
            }
        }.store(in: &self.cancellables)
    }
    
    /// Test post external barcode event
    func testPostExternalBarcodeEvent() {
        coniq.app.postExternalEvent(programKey: testProgramKey, barcode: testBarcode, event: "some_event").sink { postEventState in
            self.setResponseAlert(requestState: postEventState, description: "Customer post external event")
        }.store(in: &cancellables)
    }
    
    /// Test post external barcode event
    func testCustomerPostVisit() {
        coniq.customer.postCustomerVisit(type: .VISIT, eventOn: Date()).sink { postEventState in
            self.setResponseAlert(requestState: postEventState, description: "Customer post visit")
        }.store(in: &cancellables)
    }
    
    /// Test customer save token
    func testCustomerSaveToken(_ token: String) {
        coniq.customer.saveToken(token: token, appId: Bundle.main.bundleIdentifier!).sink { postEventState in
            self.setResponseAlert(requestState: postEventState, description: "Customer save token")
        }.store(in: &cancellables)
    }
    
    /// Test customer save device settings
    func testCustomerSaveDeviceSettings(nPermission: Bool, lPermission: Bool, cPermission: Bool) {
        coniq.customer.postDeviceSettings(
            appId: Bundle.main.bundleIdentifier!,
            notificationPermission: nPermission ? .enabled : .disabled,
            locationPermission: lPermission ? .whileInUse : .never,
            cameraPermission: cPermission ? .always : .never).sink { postDeviceSettingsState in
            self.setResponseAlert(requestState: postDeviceSettingsState, description: "Customer save device settings")
        }.store(in: &cancellables)
    }
    
    /// Test customer obfusacate
    func testCustomerObfuscate() {
        coniq.customer.obfuscate().sink { obfuscateState in
            self.setResponseAlert(requestState: obfuscateState, description: "Customer obfuscate")
        }.store(in: &cancellables)
    }
    
    /// Test logout
    func testCustomerLogout() {
        coniq.customer.logout().sink { logoutState in
            self.setResponseAlert(requestState: logoutState, description: "Customer logout")
        }.store(in: &cancellables)
    }
    
    // MARK: // ** PROGRAM  ** //
    
    /// Test get program rewards
    func testGetProgramRewards() {
        coniq.program.getRewards(programKey: testProgramKey).sink { getRewardsState in
            self.setResponseAlert(requestState: getRewardsState, description: "Get Customer Program Rewards")
        }.store(in: &cancellables)
    }
    
    /// Test get program locations
    func testGetProgramLocations() {
        coniq.program.getLocations(programKey: testProgramKey).sink { getOrdersState in
            self.setResponseAlert(requestState: getOrdersState, description: "Get Customer Program Locations")
        }.store(in: &cancellables)
    }
    
    /// Test get program location groups
    func testGetProgramLocationGroups() {
        coniq.program.getLocationGroups().sink { getOrdersState in
            self.setResponseAlert(requestState: getOrdersState, description: "Get Customer Program Location Groups")
        }.store(in: &cancellables)
    }
    
    /// Test get program available offers
    func testGetProgramAvailableOffers() {
        coniq.program.getAvailableOffers(programKey: testProgramKey).sink { getOffersState in
            self.setResponseAlert(requestState: getOffersState, description: "Get Customer Available Program Offers")
        }.store(in: &cancellables)
    }
    
    /// Test get program points events
    func testGetProgramPointsEvents() {
        coniq.program.getPointsEvents(programKey: testProgramKey).sink { getOffersState in
            self.setResponseAlert(requestState: getOffersState, description: "Get Customer Program Points Events")
        }.store(in: &cancellables)
    }
    
    // MARK: // ** RECEIPT  ** //
    
    /// Post a new receipt for customer
    func testPostCustomer(receipt: UIImage) {
        coniq.receipt.postReceipt(receipt: receipt, programKey: testProgramKey, locationId: 123, amount: 7.68, dateRedeemed: Date()).sink { postReceiptState in
            self.setResponseAlert(requestState: postReceiptState, description: "Post Customer Receipt")
        }.store(in: &self.cancellables)
    }
    
    
    /// Test get customer receipts
    func testGetCustomerReceipts() {
        coniq.receipt.getReceipts().sink { getReceiptsState in
            self.setResponseAlert(requestState: getReceiptsState, description: "Get Customer Receipts")
        }.store(in: &cancellables)
    }
    
    // MARK: // ** PAYMENT  ** //
    
    /// Test GET customer linked payment
    func testGetCustomerPaymentRedirectUrl() {
        coniq.payment.getPaymentRedirectUrl().sink { paymentState in
            self.setResponseAlert(requestState: paymentState, description: "Get linked payment")
        }.store(in: &cancellables)
    }
    
    /// Test GET customer linked payments
    func testGetCustomerLinkedPayments() {
        coniq.payment.getLinkedPayments().sink { paymentsState in
            self.setResponseAlert(requestState: paymentsState, description: "Get linked payments")
        }.store(in: &cancellables)
    }
    
    /// Test GET boosters
    func testGetBoosters() {
        coniq.customer.getAvailableBoosters().sink { boostersState in
            self.setResponseAlert(requestState: boostersState, description: "Get boosters")
        }.store(in: &cancellables)
    }

    /// Test get the customer boosters and initiate a stripe payment session for the fist customer booster
    func testGetBoostersAndInitiatePayment(successUrl: String, cancelUrl: String) {
        coniq.customer.getAvailableBoosters().sink { availableBoostersState in
            switch availableBoostersState {
            case .success(let boosters):
                if let priceId = boosters.first?.externalId {
                    self.coniq.payment.initiatePayment(priceId: priceId, successUrl: successUrl, cancelUrl: cancelUrl).sink { paymentInitState in
                        self.setResponseAlert(requestState: paymentInitState, description: "Initiate stripe payment")
                    }.store(in: &self.cancellables)
                } else {
                    self.setResponseAlert(requestState: availableBoostersState, description: "Get boosters")
                }
                
            case .failure(let error):
                self.setErrorAlert(error)
                
            default:
                break
            }
        }.store(in: &cancellables)
    }
}

// MARK: - Private methods

private extension ConiqSdkTest {
    
    func setErrorAlert(_ error: Error?) {
        if let err = error as? NetworkError {
            if let data = try? JSONSerialization.data(withJSONObject: err.body) {
                alertDetails = ("HTTP Error \(err.httpCode)", data.asJsonPrettyPrinted ?? "")
            } else {
                alertDetails = ("Error \(err.httpCode)", "Empty body")
            }
        }
    }
    
    func setSuccessAlert(title: String = "Succeed ✅", description: String) {
        alertDetails = (title, description)
    }
    
    func setResponseAlert<T: Any>(requestState: RequestState<T>, description: String) {
        if case .success = requestState {
            setSuccessAlert(description: description)
        }
        if case .failure(let error) = requestState {
            setErrorAlert(error)
        }
    }
}
