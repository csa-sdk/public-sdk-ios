// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.2 (swiftlang-5.7.2.135.5 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios15.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Coniq
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Combine
import Foundation
import Network
import Security
import Swift
import UIKit
import _Concurrency
import _StringProcessing
final public class Coniq {
  public struct Configurations {
    public let printConsoleApiLogs: Swift.Bool
    public init(printConsoleApiLogs: Swift.Bool = true)
  }
  final public var app: AppHandler {
    get
  }
  final public var authentication: AuthenticationHandler {
    get
  }
  final public var customer: CustomerHandler {
    get
  }
  final public var receipt: ReceiptHandler {
    get
  }
  final public var payment: PaymentHandler {
    get
  }
  final public var program: ProgramHandler {
    get
  }
  public init(environment: Environment, configurations: Configurations? = nil)
  final public func changeConfigurations(with newConfigurations: Configurations)
  @objc deinit
}
extension Foundation.Data {
  public var asJsonPrettyPrinted: Swift.String? {
    get
  }
}
@_hasMissingDesignatedInitializers final public class AppHandler {
  final public func getSchemes() -> Result<Schemes>
  final public func getScheme(publicId: Swift.String) -> Result<Scheme>
  final public func getTranslations() -> Result<Translations>
  final public func postExternalEvent(programKey: Swift.String, barcode: Swift.String, event: Swift.String) -> Result<SuccessResponse>
  final public func getAppConfig(appId: Swift.String) -> Result<AppConfig>
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class AuthenticationHandler {
  final public func retrieveSignUpDefinitionForm(hash: Swift.String) -> Result<SignUpDefinitionForm>
  final public func register(apiHash: Swift.String, newCustomer: NewCustomer) -> Result<RegisteredCustomer>
  final public func login(email: Swift.String, password: Swift.String) -> Result<Customer>
  final public func socialLogin(type: SocialType, email: Swift.String, password: Swift.String) -> Result<Customer>
  final public func setSocialIdentifier(type: SocialType, identifier: Swift.String) -> Result<Swift.Void>
  final public func setSocialIdentifierForPublicId(type: SocialType, publicId: Swift.String, identifier: Swift.String) -> Result<Swift.Void>
  final public func resetCredentials(email: Swift.String, redirectUrl: Swift.String? = nil) -> Result<Swift.Void>
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class CustomerHandler {
  final public func setPassword(for registeredCustomer: RegisteredCustomer, password: Swift.String) -> Result<Swift.Void>
  final public func updatePassword(for customer: Customer, currentPassword: Swift.String, newPassword: Swift.String) -> Result<Swift.Void>
  final public func updateProfile(newProfile: UpdateCustomer) -> Result<Swift.Void>
  final public func exists(barcode: Swift.String? = nil, email: Swift.String? = nil) -> Result<CustomerExist>
  final public func getTranslations() -> Result<Translations>
  final public func getAvailableBoosters() -> Result<[Booster]>
  final public func getRedeemedOffers(programKey: Swift.String) -> Result<[RedeemedOffer]>
  final public func getSubscription(programKey: Swift.String) -> Result<Subscription>
  final public func getBasicSubscription(programKey: Swift.String, barcode: Swift.String) -> Result<BasicSubscription>
  final public func getOrders() -> Result<[Order]>
  final public func getReferralSchemeCode(publicId: Swift.String) -> Result<ReferralScheme>
  final public func getFavoriteOffersIds(programKey: Swift.String) -> Result<[Swift.Int]>
  final public func saveFavoriteOffer(programKey: Swift.String, offerId: Swift.Int) -> Result<Swift.Void>
  final public func deleteFavoriteOffer(programKey: Swift.String, offerId: Swift.Int) -> Result<Swift.Void>
  final public func postCustomerVisit(type: CustomerVisitType, eventOn: Foundation.Date, deviceVisitId: Swift.String? = nil, geofences: [Swift.String]? = nil, transactionId: Swift.Int? = nil, metadata: [Swift.String : Any]? = nil, transactionKeyId: Swift.String? = nil) -> Result<Swift.Void>
  final public func postDeviceSettings(appId: Swift.String, notificationPermission: NotificationPermission? = nil, locationPermission: LocationPermission? = nil, locationAccuracy: LocationAccuracy? = nil, locationServices: LocationServices? = nil, cameraPermission: CameraPermission? = nil, nfcAvailability: NFCAvailability? = nil) -> Result<SuccessResponse>
  final public func saveToken(token: Swift.String, appId: Swift.String) -> Result<Swift.Void>
  final public func obfuscate() -> Result<SuccessResponse>
  final public func logout() -> Result<Swift.Void>
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class PaymentHandler {
  final public func initiatePayment(priceId: Swift.String, successUrl: Swift.String, cancelUrl: Swift.String) -> Result<PaymentInit>
  final public func getPaymentRedirectUrl() -> Result<LinkedPayment>
  final public func getLinkedPayments() -> Result<[Payment]>
  final public func deletePayment(method: Swift.String) -> Result<Swift.Void>
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class ProgramHandler {
  final public func getRewards(programKey: Swift.String) -> Result<[Offer]>
  final public func getLocations(programKey: Swift.String, locationGroupKey: Swift.String? = nil) -> Result<[Location]>
  final public func getLocationGroups(name: Swift.String? = nil, type: Swift.String? = nil, key: Swift.String? = nil) -> Result<[LocationGroup]>
  final public func getProgramOffers(programKey: Swift.String, language: Language? = nil, photoW: Swift.Int? = nil, photoH: Swift.Int? = nil, logoW: Swift.Int? = nil, logoH: Swift.Int? = nil) -> Result<[Offer]>
  final public func getLoyaltyProgram(programKey: Swift.String, language: Language? = nil) -> Result<LoyaltyProgram>
  final public func getAvailableOffers(programKey: Swift.String) -> Result<[ProgramOffer]>
  final public func getPointsEvents(programKey: Swift.String, language: Language? = nil, offset: Swift.Int? = nil) -> Result<PointsEvent>
  final public func spendLinkedOffer(programKey: Swift.String, ruleId: Swift.Int) -> Result<Swift.Void>
  final public func spendRedeemLinkedOffer(programKey: Swift.String, ruleId: Swift.Int, amount: Swift.Double, locationId: Swift.Int) -> Result<SpendLinkedOfferRule>
  final public func getProgramBasicSubscription(programKey: Swift.String, externalId: Swift.String) -> Result<BasicSubscription>
  final public func getCustomerPointsEvents(programKey: Swift.String, externalId: Swift.String, language: Language? = nil, offset: Swift.Int? = nil) -> Result<PointsEvent>
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class ReceiptHandler {
  final public func getReceipts() -> Result<[Receipt]>
  final public func postReceipt(receipt: UIKit.UIImage, programKey: Swift.String, locationId: Swift.Int? = nil, amount: Swift.Float? = nil, dateRedeemed: Foundation.Date? = nil) -> Result<NewPostReceipt>
  @objc deinit
}
public struct AppConfig : Swift.Codable {
  public struct Signup : Swift.Codable {
    public let hash: Swift.String
    public let language: Language
    public let forActivation: Swift.Bool
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public struct Setting : Swift.Codable {
    public let minAge: Swift.Int
    public let languages: [Language]
    public let hasStripe: Swift.Bool
    public let hasWallet: Swift.Bool
    public let showTiers: Swift.Bool
    public let redeemFlow: Swift.String
    public let barcodeUsage: Swift.String
    public let currencyDisabled: Swift.Bool
    public let hasOffersFilter: Swift.String
    public let secondsForRedeem: Swift.Int
    public let geofencingDisabled: Swift.Bool
    public let offerCarouselLimit: Swift.Int
    public let hasFavouritingOffers: Swift.Bool
    public let redemptionLocationId: Swift.Int
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public struct ProfileSection : Swift.Codable {
    public struct Section : Swift.Codable {
      public let nameLocKey: Swift.String
      public let link: Swift.String
      public func encode(to encoder: Swift.Encoder) throws
      public init(from decoder: Swift.Decoder) throws
    }
    public let `self`: AppConfig.ProfileSection.Section?
    public let sub: [AppConfig.ProfileSection.Section]?
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let programKey: Swift.String
  public let signups: [AppConfig.Signup]
  public let settings: AppConfig.Setting
  public let additionalProfileSections: [AppConfig.ProfileSection]
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct BasicSubscription : Swift.Codable {
  public let name: Swift.String
  public let surname: Swift.String
  public let tier: Swift.String
  public let currentBalance: Swift.Int
  public let lifetimeBalance: Swift.Int
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Booster : Swift.Codable {
  public struct Program : Swift.Codable {
    public let id: Swift.Int
    public let title: Swift.String
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public struct Metadata : Swift.Codable {
    public let price: Swift.String
    public let term: Swift.String
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public struct Effect : Swift.Codable {
    public struct Tier : Swift.Codable {
      public let id: Swift.Int
      public let label: Swift.String
      public func encode(to encoder: Swift.Encoder) throws
      public init(from decoder: Swift.Decoder) throws
    }
    public let expiry: Swift.Int?
    public let tier: Booster.Effect.Tier?
    public let paid: Swift.Bool?
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let id: Swift.Int
  public let program: Booster.Program
  public let description: Swift.String
  public let metadata: Booster.Metadata
  public let name: Swift.String
  public let externalId: Swift.String
  public let effects: [Booster.Effect]
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Coordinates : Swift.Codable {
  public let lat: Swift.String?
  public let lng: Swift.String?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public enum Country : Swift.String, Swift.CaseIterable, Swift.Codable {
  case AF
  case AL
  case DZ
  case AS
  case AD
  case AO
  case AI
  case AQ
  case AG
  case AR
  case AM
  case AW
  case AU
  case AT
  case AZ
  case BS
  case BH
  case BD
  case BB
  case BY
  case BE
  case BZ
  case BJ
  case BM
  case BT
  case BO
  case BA
  case BW
  case BV
  case BR
  case IO
  case BN
  case BG
  case BF
  case BI
  case KH
  case CM
  case CA
  case CV
  case KY
  case CF
  case TD
  case CL
  case CN
  case CX
  case CC
  case CO
  case KM
  case CG
  case CD
  case CK
  case CR
  case CI
  case HR
  case CU
  case CY
  case CZ
  case DK
  case DJ
  case DM
  case DO
  case EC
  case EG
  case SV
  case GQ
  case ER
  case EE
  case ET
  case FK
  case FO
  case FJ
  case FI
  case FR
  case GF
  case PF
  case TF
  case GA
  case GM
  case GE
  case DE
  case GH
  case GI
  case GR
  case GL
  case GD
  case GP
  case GU
  case GT
  case GG
  case GN
  case GW
  case GY
  case HT
  case HM
  case VA
  case HN
  case HK
  case HU
  case IS
  case IN
  case ID
  case IR
  case IQ
  case IE
  case IM
  case IL
  case IT
  case JM
  case JP
  case JE
  case JO
  case KZ
  case KE
  case KI
  case KP
  case KR
  case KW
  case KG
  case LA
  case LV
  case LB
  case LS
  case LR
  case LY
  case LI
  case LT
  case LU
  case MO
  case MK
  case MG
  case MW
  case MY
  case MV
  case ML
  case MT
  case MH
  case MQ
  case MR
  case MU
  case YT
  case MX
  case FM
  case MD
  case MC
  case MN
  case ME
  case MS
  case MA
  case MZ
  case MM
  case NA
  case NR
  case NP
  case NL
  case AN
  case NC
  case NZ
  case NI
  case NE
  case NG
  case NU
  case NF
  case MP
  case NO
  case OM
  case PK
  case PW
  case PS
  case PA
  case PG
  case PY
  case PE
  case PH
  case PN
  case PL
  case PT
  case PR
  case QA
  case RE
  case RO
  case RU
  case RW
  case SH
  case KN
  case LC
  case PM
  case VC
  case WS
  case SM
  case ST
  case SA
  case SN
  case RS
  case SC
  case SL
  case SG
  case SK
  case SI
  case SB
  case SO
  case ZA
  case GS
  case SS
  case ES
  case LK
  case SD
  case SR
  case SJ
  case SZ
  case SE
  case CH
  case SY
  case TW
  case TJ
  case TZ
  case TH
  case TL
  case TG
  case TK
  case TO
  case TT
  case TN
  case TR
  case TM
  case TC
  case TV
  case UG
  case UA
  case AE
  case GB
  case US
  case UM
  case UY
  case UZ
  case VU
  case VE
  case VN
  case VG
  case VI
  case WF
  case EH
  case YE
  case ZM
  case ZW
  public var code: Swift.Int {
    get
  }
  public static let prefixes: [Swift.String : Swift.Int]
  public init?(rawValue: Swift.String)
  public typealias AllCases = [Country]
  public typealias RawValue = Swift.String
  public static var allCases: [Country] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public struct Customer : Swift.Codable {
  public let publicId: Swift.String
  public let title: Swift.String?
  public let firstName: Swift.String?
  public let lastName: Swift.String?
  public let email: Swift.String
  public let status: Swift.String
  public let address1: Swift.String?
  public let address2: Swift.String?
  public let city: Swift.String?
  public let county: Swift.String?
  public let postcode: Swift.String?
  public let cuntry: Swift.String?
  public let company: Swift.String?
  public let gender: Gender?
  public let marketingAgreement: Swift.Bool
  public let allowEmail: Swift.Bool
  public let allowPhone: Swift.Bool
  public let allowPushNotification: Swift.Bool
  public let allowPost: Swift.Bool
  public let allowThirdParty: Swift.Bool
  public let profilingAgreement: Swift.Bool
  public let phone: Phone
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct CustomerExist : Swift.Codable {
  public let customerIdentifier: Swift.String
  public let hasCredentials: Swift.Bool
  public let programKeys: [Swift.String]
  public let identifiers: [Swift.String]
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public enum NotificationPermission : Swift.String {
  case enabled
  case disabled
  case unknown
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum LocationPermission : Swift.String {
  case always
  case whileInUse
  case never
  case unknown
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public typealias LocationAccuracy = NotificationPermission
public typealias LocationServices = NotificationPermission
public enum CameraPermission : Swift.String {
  case always
  case never
  case unknown
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum NFCAvailability : Swift.String {
  case enabled
  case disabled
  case missing
  case unknown
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum Gender : Swift.String, Swift.Codable {
  case male
  case female
  case other
  case preferNotSay
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum Language : Swift.String, Swift.CaseIterable, Swift.Codable {
  case CA
  case CS
  case DE
  case EN
  case ES
  case FI
  case FR
  case IT
  case NL
  case NO
  case PL
  case PT
  case RU
  case SV
  public init?(rawValue: Swift.String)
  public typealias AllCases = [Language]
  public typealias RawValue = Swift.String
  public static var allCases: [Language] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public struct Location : Swift.Codable {
  public let id: Swift.Int
  public let name: Swift.String
  public let coordinates: Coordinates
  public let logoUrl: Swift.String?
  public let createdDatetimeUtc: Swift.String
  public let modifiedDatetimeUtc: Swift.String
  public let locationGroupIds: [Swift.Int]?
  public let hasRedemptionKeys: Swift.Bool
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct LocationGroup : Swift.Codable {
  public struct Content : Swift.Codable {
    public struct Image : Swift.Codable {
      public let path: Swift.String
      public let url: Swift.String
      public func encode(to encoder: Swift.Encoder) throws
      public init(from decoder: Swift.Decoder) throws
    }
    public let image: LocationGroup.Content.Image
    public let location: Swift.String?
    public let coordinates: Coordinates
    public let description: [Swift.String : Swift.String]
    public let fullAddress: [Swift.String]
    public let contactEmail: Swift.String
    public let contactTelephoneNumber: Swift.String?
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let id: Swift.Int
  public let name: Swift.String
  public let locationIds: [Swift.Int]
  public let type: Swift.String
  public let content: LocationGroup.Content?
  public let timezone: Swift.String?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct LoyaltyProgram : Swift.Codable {
  public struct Tier : Swift.Codable {
    public let id: Swift.Int
    public let label: Swift.String
    public let daysToRemain: Swift.Int?
    public let manualAssignment: Swift.Bool
    public let minimumPoints: Swift.Int
    public let minimumSpend: Swift.Int
    public let pointsSince: Swift.Int?
    public let spendSince: Swift.Int?
    public let offers: [Swift.String : [Swift.String : Swift.Int]]
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public struct PointsSpendLinkedOfferRule : Swift.Codable {
    public let startDate: Swift.String
    public let ruleId: Swift.Int
    public let pointsRequired: Swift.Int
    public let offerType: Swift.String
    public let offer: Offer
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let id: Swift.Int
  public let title: Swift.String
  public let offerType: Swift.String
  public let tiers: [LoyaltyProgram.Tier]
  public let startDate: Swift.String
  public let pointsSpendLinkedOfferRules: [LoyaltyProgram.PointsSpendLinkedOfferRule]
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Offer : Swift.Codable {
  public struct Tier : Swift.Codable {
    public let label: Swift.String
    public let manualAssignment: Swift.Bool
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let id: Swift.Int
  public let title: Swift.String
  public let description: Swift.String
  public let shortDescription: Swift.String
  public let startDate: Swift.String
  public let validDays: [Swift.Int]
  public let validStartTime: Swift.String
  public let validEndTime: Swift.String
  public let createdDatetimeUtc: Swift.String
  public let modifiedDatetimeUtc: Swift.String
  public let usage: Swift.String
  public let tags: [Swift.String]?
  public let tiers: [Offer.Tier]?
  public let photoUrl: Swift.String?
  public let photoMediumUrl: Swift.String?
  public let photoThumbnailUrl: Swift.String?
  public let locationIds: [Swift.Int]
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Order : Swift.Codable {
  public struct Booster : Swift.Codable {
    public let id: Swift.Int
    public let name: Swift.String
    public let description: Swift.String
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public struct Payment : Swift.Codable {
    public let publicId: Swift.String
    public let externalId: Swift.String
    public let status: Swift.String
    public let createdOn: Swift.String
    public let updatedOn: Swift.String
    public let amount: Swift.String
    public let amountUnits: Swift.String
    public let currencyCode: Swift.String
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let publicId: Swift.String
  public let status: Swift.String
  public let booster: Order.Booster
  public let payments: [Order.Payment]
  public let createdOn: Swift.String
  public let updatedOn: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct LinkedPayment : Swift.Codable {
  public let redirectUrl: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Payment : Swift.Codable {
  public let id: Swift.String
  public let supplier: Swift.String
  public let supplierPaymentMethodId: Swift.String
  public let linkedOn: Swift.String
  public let paymentProvider: Swift.String
  public let safeIdentifier: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct PaymentInit : Swift.Codable {
  public let id: Swift.Int
  public let externalId: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Phone : Swift.Codable {
  public let countryCode: Swift.String
  public let number: Swift.String
  public init(countryCode: Swift.String, number: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct PointsEvent : Swift.Codable {
  public struct Event : Swift.Codable {
    public struct PersistentPoints : Swift.Codable {
      public let type: Swift.String
      public let awardedOn: Swift.String
      public let createdOn: Swift.String
      public func encode(to encoder: Swift.Encoder) throws
      public init(from decoder: Swift.Decoder) throws
    }
    public let id: Swift.Int
    public let loyalty: Swift.String
    public let reason: Swift.String
    public let points: Swift.Int
    public let createdOn: Swift.String
    public let persistentPoints: PointsEvent.Event.PersistentPoints
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let pointsEvents: [PointsEvent.Event]
  public let total: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct ProgramOffer : Swift.Codable {
  public let startDate: Swift.String
  public let ruleId: Swift.Int
  public let pointsRequired: Swift.Int
  public let offerType: Swift.String
  public let offer: Offer
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct NewPostReceipt : Swift.Codable {
  public let receiptId: Swift.Int
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Receipt : Swift.Codable {
  public let id: Swift.Int
  public let location: Swift.String?
  public let locationId: Swift.Int?
  public let status: Swift.String
  public let transactionId: Swift.Int?
  public let price: Swift.Double?
  public let createdOn: Swift.String
  public let updatedOn: Swift.String?
  public let receiptImage: Swift.String
  public let customer: Swift.String
  public let programKey: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct RedeemedOffer : Swift.Codable {
  public let ruleId: Swift.Int
  public let offerType: Swift.String
  public let points: Swift.Int
  public let createdOn: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct ReferralScheme : Swift.Codable {
  public struct ReferralCode : Swift.Codable {
    public struct CodeUsage : Swift.Codable {
      public let name: Swift.String
      public let date: Swift.String
      public func encode(to encoder: Swift.Encoder) throws
      public init(from decoder: Swift.Decoder) throws
    }
    public let referralSchemePublicId: Swift.String
    public let customerId: Swift.String
    public let code: Swift.String
    public let numberOfUses: Swift.Int
    public let codeUsage: [ReferralScheme.ReferralCode.CodeUsage]
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let referralCode: ReferralScheme.ReferralCode
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct RegisteredCustomer : Swift.Codable {
  public struct Fields : Swift.Codable {
    public let firstName: Swift.String
    public let email: Swift.String
    public var lastName: Swift.String?
    public var phone: Phone?
    public var address1: Swift.String?
    public var address2: Swift.String?
    public var city: Swift.String?
    public var county: Swift.String?
    public var postcode: Swift.String?
    public var company: Swift.String?
    public var country: Country?
    public var dateOfBirth: Swift.String?
    public var gender: Gender?
    public var externalId: Swift.String?
    public var barcodeNumber: Swift.String?
    public var preferredLocationGroup: Swift.Int?
    public var preferredLanguage: Language?
    public var privacyAgreement: Swift.Bool?
    public var marketingAgreement: Swift.Bool?
    public var profilingAgreement: Swift.Bool?
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let message: Swift.String
  public let publicId: Swift.String
  public let fields: RegisteredCustomer.Fields
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Scheme : Swift.Codable {
  public struct Award : Swift.Codable {
    public let type: Swift.String
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let name: Swift.String
  public let loyaltyProgramName: Swift.String
  public let loyaltyProgramId: Swift.Int
  public let endDate: Swift.String?
  public let points: Swift.Int
  public let limit: Swift.Int
  public let publicId: Swift.String
  public let awardReferrer: Swift.Bool
  public let awardReferredCustomer: Swift.Bool
  public let awards: [Scheme.Award]
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Schemes : Swift.Codable {
  public let referralSchemes: [Scheme]
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct SignUpDefinitionForm : Swift.Codable {
  public struct Account : Swift.Codable {
    public let name: Swift.String
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public struct PolicyDetail : Swift.Codable {
    public struct Policy : Swift.Codable {
      public let linkText: Swift.String
      public let url: Swift.String
      public func encode(to encoder: Swift.Encoder) throws
      public init(from decoder: Swift.Decoder) throws
    }
    public let linkText: Swift.String
    public let policies: [SignUpDefinitionForm.PolicyDetail.Policy]
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public struct Field : Swift.Codable {
    public struct Option : Swift.Codable {
      public let value: Swift.String
      public let text: Swift.String
      public func encode(to encoder: Swift.Encoder) throws
      public init(from decoder: Swift.Decoder) throws
    }
    public let name: Swift.String
    public let type: Swift.String
    public let required: Swift.Bool
    public let label: Swift.String
    public let fields: [SignUpDefinitionForm.Field]?
    public let strict: Swift.Bool?
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let hash: Swift.String
  public let name: Swift.String
  public let message: Swift.String
  public let privacyPolicyLink: Swift.String
  public let language: Swift.String
  public let bgColor: Swift.String
  public let fgColor: Swift.String
  public let account: SignUpDefinitionForm.Account
  public let policyDetail: SignUpDefinitionForm.PolicyDetail
  public let fields: [SignUpDefinitionForm.Field]
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct SpendLinkedOfferRule : Swift.Codable {
  public let success: Swift.Bool
  public let transactionId: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Subscription : Swift.Codable {
  public struct Tier : Swift.Codable {
    public let label: Swift.String
    public let expiryDate: Swift.String?
    public let assignedOn: Swift.String
    public let spentSinceJoin: Swift.Int
    public let earnedSinceJoin: Swift.Int
    public func encode(to encoder: Swift.Encoder) throws
    public init(from decoder: Swift.Decoder) throws
  }
  public let barcodeNumber: Swift.String
  public let barcodeImageUrl: Swift.String
  public let barcodeImages: [Swift.String : Swift.String]
  public let title: Swift.String
  public let firstName: Swift.String?
  public let tier: Subscription.Tier
  public let balance: Swift.Int
  public let subscriptionStartDate: Swift.String
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct SuccessResponse : Swift.Codable {
  public let success: Swift.Bool
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Translations : Swift.Codable {
  public let success: Swift.Bool
  public let translation: [Swift.String : Any]
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public enum CustomerVisitType : Swift.String {
  case ENTER
  case LEAVE
  case DWELL
  case VISIT
  case TRANSACT
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum Environment {
  public enum Area {
    case EU
    case US
    public static func == (a: Environment.Area, b: Environment.Area) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  case staging(clientId: Swift.String, clientSecret: Swift.String)
  case preProduction(clientId: Swift.String, clientSecret: Swift.String, area: Environment.Area)
  case production(clientId: Swift.String, clientSecret: Swift.String, area: Environment.Area)
}
public struct NewCustomer : Swift.Codable {
  public let firstName: Swift.String
  public let email: Swift.String
  public var lastName: Swift.String?
  public var phone: Phone?
  public var address1: Swift.String?
  public var address2: Swift.String?
  public var city: Swift.String?
  public var county: Swift.String?
  public var postcode: Swift.String?
  public var company: Swift.String?
  public var country: Swift.String?
  public var dateOfBirth: Foundation.Date?
  public var gender: Gender?
  public var externalId: Swift.String?
  public var barcodeNumber: Swift.String?
  public var preferredLocationGroup: Swift.Int?
  public var preferredLanguage: Language?
  public var privacyAgreement: Swift.Bool?
  public var marketingAgreement: Swift.Bool?
  public var profilingAgreement: Swift.Bool?
  public init(firstName: Swift.String, email: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public enum SocialType : Swift.String {
  case google
  case facebook
  case apple
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct UpdateCustomer : Swift.Codable {
  public var firstName: Swift.String?
  public var lastName: Swift.String?
  public var address1: Swift.String?
  public var address2: Swift.String?
  public var city: Swift.String?
  public var phone: Phone?
  public var marketingAgreement: Swift.Bool?
  public var allowEmail: Swift.Bool?
  public var allowPhone: Swift.Bool?
  public var allowPushNotification: Swift.Bool?
  public var allowPost: Swift.Bool?
  public var allowThirdParty: Swift.Bool?
  public var profilingAgreement: Swift.Bool?
  public init()
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct NetworkError : Swift.Error {
  public let httpCode: Swift.Int
  public let body: [Swift.String : Any]
}
public typealias Result<T> = Combine.AnyPublisher<RequestState<T>, Swift.Never>
public enum RequestState<Type> {
  case idle
  case failure(Swift.Error)
  case success(Type)
  public var isIdle: Swift.Bool {
    get
  }
  public var succeeded: Swift.Bool {
    get
  }
  public var failed: Swift.Bool {
    get
  }
}
extension Country : Swift.Equatable {}
extension Country : Swift.Hashable {}
extension Country : Swift.RawRepresentable {}
extension NotificationPermission : Swift.Equatable {}
extension NotificationPermission : Swift.Hashable {}
extension NotificationPermission : Swift.RawRepresentable {}
extension LocationPermission : Swift.Equatable {}
extension LocationPermission : Swift.Hashable {}
extension LocationPermission : Swift.RawRepresentable {}
extension CameraPermission : Swift.Equatable {}
extension CameraPermission : Swift.Hashable {}
extension CameraPermission : Swift.RawRepresentable {}
extension NFCAvailability : Swift.Equatable {}
extension NFCAvailability : Swift.Hashable {}
extension NFCAvailability : Swift.RawRepresentable {}
extension Gender : Swift.Equatable {}
extension Gender : Swift.Hashable {}
extension Gender : Swift.RawRepresentable {}
extension Language : Swift.Equatable {}
extension Language : Swift.Hashable {}
extension Language : Swift.RawRepresentable {}
extension CustomerVisitType : Swift.Equatable {}
extension CustomerVisitType : Swift.Hashable {}
extension CustomerVisitType : Swift.RawRepresentable {}
extension Environment.Area : Swift.Equatable {}
extension Environment.Area : Swift.Hashable {}
extension SocialType : Swift.Equatable {}
extension SocialType : Swift.Hashable {}
extension SocialType : Swift.RawRepresentable {}
